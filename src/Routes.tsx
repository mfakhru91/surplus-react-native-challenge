import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { CardStyleInterpolators, createStackNavigator } from '@react-navigation/stack'
import * as Screens from './screens'
import { useAppSelector } from './redux/hooks'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {BottomTab as BottomTabComponent} from './components/molecules'
export type RouteParamsList = {
    SplashScreen:undefined,
    AuthScreen:undefined,
    SingInScreen:undefined,
    SingUpScreen:undefined,
    MainScreen:undefined,
    ProfileScreen:undefined,
    BottomTab:undefined,
    DetailMovieScreen:{
      id:string
    },
    SearchScreen:{
      key:string
    }
}

const Stack = createStackNavigator<RouteParamsList>()

const Tab = createBottomTabNavigator<RouteParamsList>();
const BottomTab = () => {
  return (
    <Tab.Navigator
      tabBar={props => <BottomTabComponent {...props}/>}
      screenOptions={{
        headerShown:false
      }}>
      <Tab.Screen name="MainScreen" component={Screens.MainScreen} />
      <Tab.Screen name="ProfileScreen" component={Screens.ProfileScreen}/>
    </Tab.Navigator>
  )
}

const Routes = () => {
  const {auth} = useAppSelector(state=>state)
  return (
    <Stack.Navigator
        screenOptions={{
            headerShown:false
        }}>   
        {!auth.isLogin?(
          <Stack.Group>
            <Stack.Screen name="SplashScreen" component={Screens.SplashScreen}/>   
            <Stack.Screen name="AuthScreen" component={Screens.Auth.AuthScreen}/>
            <Stack.Screen name="SingInScreen" component={Screens.Auth.SignInScreen}/>    
            <Stack.Screen name="SingUpScreen" component={Screens.Auth.SignUpScreen}/>    
          </Stack.Group>
        ):(
          <Stack.Group>
            <Stack.Screen name="BottomTab" component={BottomTab} />
            <Stack.Screen 
            options={{
               cardStyleInterpolator:CardStyleInterpolators.forModalPresentationIOS
            }}
            name="DetailMovieScreen" component={Screens.DetailMovieScreen}/>
            <Stack.Screen 
            options={{
               cardStyleInterpolator:CardStyleInterpolators.forHorizontalIOS
            }}
            name="SearchScreen" component={Screens.SearchScreen}/>
          </Stack.Group>          
        )}
    </Stack.Navigator>
  )
}

export default Routes

const styles = StyleSheet.create({})