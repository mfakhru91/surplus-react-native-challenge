const Logo = require("./logo.png")
const facebookLogo = require("./facebookLogo.png")
const googleLogo = require("./googleLogo.png")
export {
    Logo,
    facebookLogo,
    googleLogo
}