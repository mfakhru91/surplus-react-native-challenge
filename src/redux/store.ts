import {configureStore} from '@reduxjs/toolkit'
import * as features from './features'

const store = configureStore({
    reducer:{
        auth:features.authSlice,        
    }
})

export default store 
export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch