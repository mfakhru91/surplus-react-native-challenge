import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {AuthInterface} from './type'

const initialState: AuthInterface = {
    "isLogin":false,
}

const authSlice = createSlice({
    name:"auth",
    initialState,
    reducers:{
        setAuth:(state,action:PayloadAction<AuthInterface>)=>{
            state.isLogin = action.payload.isLogin
            state.username = action.payload.username
            state.email = action.payload.email
        },        
        logout:(state)=>{
            state.isLogin = false
        },
    }
})

export default authSlice.reducer
export const {
    setAuth,
    logout,
} = authSlice.actions