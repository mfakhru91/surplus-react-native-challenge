import api from "../.."

interface GetMovieList {
  page:string,
  search?:string
}

export const getMovieList = async (params:GetMovieList) => {
  const  {page} = params
  try {
    const {data} = await api.get(`/movie/popular?language=en-US&page=${page}`)
    return {
      "data":data,
      "hasNextPage":data.page < data.total_pages,
      "nextPage":data.page+1
    }
  } catch (error) {
    throw error
  }
}

export const searchMovieList = async (params:GetMovieList) => {
  const  {page} = params
  try {
    const {data} = await api.get(`/search/movie?query=${params.search}&include_adult=false&language=en-US&page=${page}`)
    return {
      "data":data,
      "hasNextPage":data.page < data.total_pages,
      "nextPage":data.page+1
    }
  } catch (error) {
    throw error
  }
}

export const getDetailMovie = async (id:string) => {
  try {
    const {data} = await api.get(`/movie/${id}?language=en-US`)
    return data
  } catch (error) {
    throw error
  }
}

