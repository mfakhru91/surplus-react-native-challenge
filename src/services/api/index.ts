import axios from "axios";
import {base_url,token} from '../../config.json'

const api = axios.create({
    baseURL:base_url,
    timeout:8000,
    headers:{
        'accept':'application/json'
    }
})

api.interceptors.request.use(async config =>{
    if (token) {
        config.headers.Authorization = `Bearer  ${token}`
    }
    return config
}, error => Promise.reject(error))

api.interceptors.response.use(response => {
    return response
},error => {
    const newError:any = {
        error
    }    
    if (error.response) {        
        if (error.response.data.errors) {
            const errors:any[] = error.response.data.errors
            let message = ""
            errors.map(item=>{
                Object.keys(item).map(key=>{              
                const data:any[] = item[key]
                data.map(msg=>{
                    message = message +`\n- ${msg}`
                })
                })
            })
            newError.errorMessage = message
        }else if (error.response.data.error){
            newError.errorMessage = error.response.data.error
        }else{
            newError.errorMessage = error.response.data.message
        }
    }else if (error.message){
        // console.log(error)
    }
    return Promise.reject(newError)
})


export default api;