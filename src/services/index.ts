import api from "./api";
import * as apiCollection from './api/services'
export {
    api,
    apiCollection
}