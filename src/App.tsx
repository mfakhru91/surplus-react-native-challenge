import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { GestureHandlerRootView } from 'react-native-gesture-handler'
import { NavigationContainer } from '@react-navigation/native'
import Routes from './Routes'
import {Provider as ReduxProvider} from 'react-redux'
import store from './redux/store'
import { QueryClientProvider,QueryClient } from 'react-query'

const queryClient = new QueryClient()

const App = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <ReduxProvider store={store}>
        <GestureHandlerRootView style={{flex:1}}>
          <NavigationContainer>
            <Routes/>
          </NavigationContainer>
        </GestureHandlerRootView>
      </ReduxProvider>
    </QueryClientProvider>
  )
}

export default App

const styles = StyleSheet.create({})