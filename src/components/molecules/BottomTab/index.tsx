/*
 *   Copyright (c) 2023 Muhammad Fakhru Alfarizqi
 *   All rights reserved.
 */
import { Image, StyleSheet, TouchableOpacity, View } from 'react-native'
import React, { FC } from 'react'
import { BottomTabBarProps } from '@react-navigation/bottom-tabs'
import { useTheme } from 'react-native-paper'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { useAppSelector } from '../../../redux/hooks'
import { Text } from '../../atoms'

const index:FC<BottomTabBarProps> = (props) => {
    const {
        navigation, 
        descriptors, 
        state
    } = props
    const newStyles = styles(props)    
    const {auth} = useAppSelector(state=>state)

    const customeIcon = (icon:string,title:string,isFocused:boolean) => { 
        const color = isFocused?"#FFFFFF":"#EEEEEE"
        const newStyles = styles(props,{icon,title,isFocused})
        return (
            <View style={newStyles.iconGroup}>
                <Icon name={icon} color={color} size={30}/>
                {/* <Text size={10} color={color} >{title}</Text> */}
            </View>
        )
    }    
    
    return (
        <View style={newStyles.bottomTabContainer}>
            {state.routes.map((route,index)=>{
                const {options} = descriptors[route.key]
                const isFocused = state.index === index

                // *  Define icon name for each tab
                const IconComponent:any = () => {
                    if (route.name === "MainScreen") {
                        return customeIcon("home","Home",isFocused)
                    }else if(route.name === "ProfileScreen"){
                        return customeIcon("account","Profile",isFocused)
                    }                }
                
                const onPress = () => navigation.navigate(route.name)

                return  (
                    <TouchableOpacity 
                        onPress={onPress}
                        key={route.key} style={newStyles.tabItem}>
                            <IconComponent/>     
                    </TouchableOpacity>
                )
            })}
        </View>
    )
}

export default index

const styles = ( props:BottomTabBarProps,icon?:any) => StyleSheet.create({
    bottomTabContainer:{
        paddingVertical:2,     
        backgroundColor:"#290c45",   
        justifyContent:"space-between",
        flexDirection:'row',
        paddingHorizontal:5,        
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    tabItem:{
        justifyContent:'center',
        alignItems:'center',
        padding:10,
        flex:1,
    },
    iconGroup:{
        justifyContent:'center',
        alignItems:'center',
        minWidth:70,
    },
    profileImage:{
        height:30,
        width:30,
        borderRadius:50,
    }
})