import React from "react";

export interface AuthLayoutInterface {
    title:string,
    message:string,
    headerRight?:React.ReactNode
    children:React.ReactNode,
    goBack?:()=>void
}