import { ScrollView, StyleSheet, TouchableOpacity, View } from 'react-native'
import React, { FC } from 'react'
import { AuthLayoutInterface } from './type'
import { Text } from '../../atoms'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
const index:FC<AuthLayoutInterface> = (props) => {
  return (
    <View style={styles.container} >
      <View style={styles.header}>        
        {props.goBack && (
          <Icon 
            onPress={props.goBack}
            name='arrow-left' 
            color={"#FFFFFF"} 
            size={20}/>
        )}
        {props.headerRight}
      </View>
      <View style={styles.screenInfoContainer}>
        <Text size={30} font="Poppins-Bold" color='#FFFFFF'>{props.title}</Text>
        <Text style={{textAlign:'justify'}} color='#FFFFFF'>{props.message}</Text>
      </View>
      <View style={styles.contentContainer}>
        {props.children}
      </View>
    </View>
  )
}

export default index

const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:"#6A1FB4"
  },
  screenInfoContainer:{
    padding:20,
    marginBottom:20,    
    flex:1,
    justifyContent:"flex-end"    
  },
  header:{
    height:60,
    paddingVertical:10,
    paddingHorizontal:20,
    flexDirection:'row',
    justifyContent:"space-between",
    alignItems:'center'    
  },
  contentContainer:{
    backgroundColor:"#FFFFFF",
    minHeight:300,
    paddingTop:30,
    paddingBottom:20,
    paddingHorizontal:20,
    borderTopStartRadius:20,
    borderTopEndRadius:20,
  }
})