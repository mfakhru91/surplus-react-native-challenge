import { Ref } from "react";
import { TextInput, ViewStyle } from "react-native";
import { TextInputProps } from "react-native-paper";

export interface TextInputType extends TextInputProps {
    errorMessage?:string,
    password?:boolean,
    borderRadius?:number
    forwardedRef?: Ref<TextInput>
    textInputContainer?:ViewStyle
}