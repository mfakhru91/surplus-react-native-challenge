import { StyleProp, StyleSheet, TextInput as Input, TouchableOpacity, View } from 'react-native'
import React, { FC, forwardRef, ForwardRefRenderFunction, useState } from 'react'
import { TextInput, useTheme } from 'react-native-paper'
import { TextInputType } from './type'
import { Text } from '..'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

const index:ForwardRefRenderFunction<Input,TextInputType> = (props,ref) => {
  const rest = {...props}
  const newStyles = styles(props)
  const style:any[] = [newStyles.textInput,props.style] 
  const outlineStyle = [newStyles.outlineStyle]
  const [showPassword, setShowPassword] = useState(false)  
  
  const textInputTheme = {
    colors: {
      primary: "#6A1FB4",
      placeholder: "#6A1FB4",
      text: "#000000",
      background: "#FFFFFF",
      underlineColor: 'transparent'
    }
  };
  rest.style = style
  rest.outlineStyle = outlineStyle
  rest.right = props.password&&<TextInput.Icon onPress={()=>setShowPassword(!showPassword)} icon={!showPassword?"eye-outline":"eye-off-outline"} />||props.right

  return (
    <View style={[newStyles.container,props.textInputContainer]}>
      <TextInput         
          {...rest}    
          secureTextEntry={props.password&&!showPassword}          
          theme={textInputTheme}       
          mode={props.mode||"outlined"}  
          ref={props.forwardedRef||ref} 
          />
        {props.error&&(<Text color="red" size={12}>{props.errorMessage}</Text>)}
    </View>
  )
}

export default forwardRef(index)

const styles = (props:TextInputType) => StyleSheet.create({
  container:{
    marginVertical:5,    
  },
  textInput:{
    marginBottom:5,
    fontFamily:"Poppins-Regular",
  },
  outlineStyle:{
    borderRadius:props.borderRadius?props.borderRadius:50
  }
})