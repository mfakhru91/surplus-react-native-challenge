import { StyleSheet, View } from 'react-native'
import React, { FC } from 'react'
import { Text } from 'react-native-paper'
import { TextType } from './type'

const index:FC<TextType> = (props) => {
    const {
        color,
        size,
        fontWeight,
        shadow
    } = props    
    const newStyles = styles(props)
    const style:any[] = [newStyles.text,props.style,shadow&&newStyles.shadow]
    
    const res = {...props}
    res.style = style
    return (    
        <Text {...res}/>
    )
}

export default index

const styles = (props:TextType) => StyleSheet.create({
    text:{
        fontFamily:props.font||"Poppins-Regular",
        color:props.color||"#000000",
        fontSize:props.size,
    },
    shadow:{
        shadowColor: "#000",
        shadowOffset: {
        width: 0,
        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    }
})