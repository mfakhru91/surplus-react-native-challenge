import { TextProps } from "react-native-paper";

export interface TextType extends TextProps<any> {
    color?:string
    size?:number,
    shadow?:boolean,
    fontWeight?:"normal" | "bold" | "100" | "200" | "300" | "400" | "500" | "600" | "700" | "800" | "900" ,
    font?:"Poppins-Black"|"Poppins-BlackItalic"|"Poppins-Bold"|"Poppins-BoldItalic"|"Poppins-ExtraBold"|"Poppins-ExtraBoldItalic"
        |"Poppins-ExtraLight"|"Poppins-ExtraLightItalic"|"Poppins-Italic"|"Poppins-Light"|"Poppins-LightItalic"|"Poppins-Medium"|
        "Poppins-MediumItalic"|"Poppins-Regular"|"Poppins-SemiBold"|"Poppins-SemiBoldItalic"|"Poppins-Thin"|"Poppins-ThinItalic"|
        "icomoon"
}