import  * as Auth from "./Auth";
import SplashScreen from "./SplashScreen/SplashScreen";
import MainScreen from "./MainScreen/MainScreen";
import ProfileScreen from "./ProfileScreen/ProfileScreen";
import DetailMovieScreen from "./DetailMovieScreen/DetailMovieScreen";
import SearchScreen from "./SearchScreen/SearchScreen";

export {
    SplashScreen,
    Auth,
    MainScreen,
    ProfileScreen,
    DetailMovieScreen,
    SearchScreen
}
