import { Animated, Image, StyleSheet, Text, View } from 'react-native'
import React, { FC, useEffect, useRef } from 'react'
import LottieView from 'lottie-react-native'
import { Images } from '../../assets'
import { StackScreenProps } from '@react-navigation/stack'
import { RouteParamsList } from '../../Routes'
import { useAppDispatch } from '../../redux/hooks'
import { setAuth } from '../../redux/features/auth/authSlice'
import AsyncStorage from '@react-native-async-storage/async-storage'

const background = require("./background.json")
const SplashScreen:FC<StackScreenProps<RouteParamsList,"SplashScreen">> = ({navigation}) => {
  const dispatch = useAppDispatch()
  const fadeAnim = useRef(new Animated.Value(0)).current

  const fadeInLogo = () => {
    Animated.timing(fadeAnim,{
      toValue:1,
      duration:1000,
      useNativeDriver:true
    }).start();
  }
  

  useEffect(() => {
    const animation = setTimeout(() => {
      fadeInLogo()
    }, 1500);
    const splashScreen = setTimeout(async () => {
      const data = await AsyncStorage.getItem("auth")
      if (data !== null) {
        const auth = JSON.parse(data)
        dispatch(setAuth({
          "email":auth.email,
          "isLogin":true,
          "username":auth.username
        }))
      }else{
        navigation.navigate("AuthScreen")
      }
    }, 3500);
    return () => {
      clearTimeout(animation)
      clearTimeout(splashScreen)
    }
  }, []);

  const logoOpacity = fadeAnim.interpolate({
    inputRange:[0,1],
    outputRange:[0,1]
  })

  return (
    <View style={styles.container}>
      <LottieView source={background} 
        autoPlay 
        loop={false}
        resizeMode="cover"/>    
      <View style={[styles.contentContainer]}>
        <Animated.Image source={Images.Logo} style={[styles.logo,{opacity:fadeAnim}]}/>
      </View>    
    </View>
  )
}

export default SplashScreen

const styles = StyleSheet.create({
  container:{
    flex:1,
  },
  contentContainer:{
    flex:1,
    paddingBottom:100,
    justifyContent:'center',
    alignItems:'center'
  },
  logo:{
    resizeMode:'contain',
    height:120,
    width:120,
  }
})