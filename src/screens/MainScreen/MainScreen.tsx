import { Dimensions, ScrollView, StyleSheet, View } from 'react-native'
import React,{FC, useState} from 'react'
import { useInfiniteQuery } from 'react-query'
import { apiCollection } from '../../services'
import { TouchableOpacity } from 'react-native'
import { Image } from 'react-native'
import {image_url, profile_image} from '../../config.json'
import { ImageBackground } from 'react-native'
import { Text, TextInput } from '../../components/atoms'
import { useAppSelector } from '../../redux/hooks'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {ActivityIndicator, TextInput as Input} from 'react-native-paper'
import { StackScreenProps } from '@react-navigation/stack'
import { RouteParamsList } from '../../Routes'

const {width} = Dimensions.get("screen")

const MainScreen:FC<StackScreenProps<RouteParamsList,"MainScreen">> = ({
  navigation
}) => {
  const {auth} = useAppSelector(state=>state)
  const [search, setSearch] = useState("");
  const movie = useInfiniteQuery(["movie"],{
    queryFn:({pageParam = 2})=>apiCollection.movie.getMovieList({page:pageParam}),
    getNextPageParam:(lastPage)=> {
      if (!lastPage.hasNextPage) return null
      return lastPage.nextPage
    }
  })

  const firsPage = useInfiniteQuery(["topMovie"],{
    queryFn:({pageParam = 1})=>apiCollection.movie.getMovieList({page:pageParam}),
    getNextPageParam:(lastPage)=> {
      if (!lastPage.hasNextPage) return null
      return lastPage.nextPage
    }
  })

  const firsPageList = firsPage.data?.pages.flatMap((page) => page.data.results);
  const movieList = movie.data?.pages.flatMap((page) => page.data.results);


  const topMovieRender = ({item,index}:{item:any,index:any}) => {    
    return (
      <TouchableOpacity 
        onPress={()=>navigation.navigate("DetailMovieScreen",{id:item.id})}
        key={index}>
        <ImageBackground
          style={styles.topMoviePosterContainer}
          imageStyle={styles.topMoviePoster}
          source={{uri:`${image_url}${item.poster_path}`}}>
            <View style={styles.rateContainer}>
              <Text>{item.vote_average}</Text>
            </View>
         </ImageBackground>
      </TouchableOpacity>
    )
  }

  const movieRender = ({item,index}:{item:any,index:any}) => {    
    return (
      <TouchableOpacity 
        onPress={()=>navigation.navigate("DetailMovieScreen",{id:item.id})}
        key={index}>
        <ImageBackground
          style={styles.moviePoster}
          source={{uri:`${image_url}${item.poster_path}`}}>
          <View style={styles.movieTitleContainer}>
            <Text color='#FFFFFF' >{item.original_title}</Text>
            <View style={styles.rateContainer}>
                <Text>{item.vote_average}</Text>
            </View>
          </View>
        </ImageBackground>
      </TouchableOpacity>
    )
  }

  const hendleScroll = (event:any) => {
    const offsetY = event.nativeEvent.contentOffset.y;
    const contentHeight = event.nativeEvent.contentSize.height;
    const scrollHeight = event.nativeEvent.layoutMeasurement.height;
    if (offsetY + scrollHeight >= contentHeight) {
      if (!movie.isLoading && !movie.isRefetching && !movie.isFetchingNextPage) {
        movie.fetchNextPage()
      }
    }
  }
  

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <View>
          <Text color='#FFFFFF'><Text font="Poppins-Bold" color='#FFFFFF'>Hallo</Text> {auth.username}</Text>
          <Text color='#FFFFFF' font="Poppins-Light" size={12}>look for your favorite movie</Text>
        </View>
        <Image style={styles.profileImage} source={{uri:profile_image}} />
      </View>
      
      <ScrollView
        onScroll={hendleScroll}
        showsVerticalScrollIndicator={false}>

        <TextInput
          mode="outlined"
          placeholder='Search Movies'
          value={search}
          onChangeText={text=>setSearch(text)}
          onEndEditing={()=>search !== "" && navigation.navigate("SearchScreen",{key:search})}
          right={<Input.Icon  
            onPress={()=>search !== "" && navigation.navigate("SearchScreen",{key:search})}
            icon={"magnify"} color={"#000000"}/>}/>

        <View style={styles.titleContainer}>
          <Icon name='movie-open' size={25} color={"#FFFFFF"} style={{marginRight:10}}/>
          <Text font="Poppins-Bold" size={20} color="#FFFFFF">Top <Text font="Poppins-Regular" color="#FFFFFF">Movie</Text></Text>
        </View>

        <ScrollView horizontal scrollEnabled={!firsPage.isLoading}>
          {!firsPage.isLoading ? firsPageList?.map((item,index)=>(
            topMovieRender({index,item})
          )):(
            <View style={styles.loadingTopMovie}>
              <ActivityIndicator size="large"/>
            </View>
          )} 
        </ScrollView>  
        
        <View style={styles.titleContainer}>
          <Icon name='view-list' size={25} color={"#FFFFFF"} style={{marginRight:10}}/>
          <Text font="Poppins-Bold" size={20} color="#FFFFFF">Movie <Text font="Poppins-Regular" color="#FFFFFF">List</Text></Text>
        </View>
        <View style={styles.movieContainer}>
          {!movie.isLoading ? movieList?.map((item,index)=>(
            movieRender({index,item})
          )):(
            <View style={styles.loadingMovieList}>
              <ActivityIndicator size="large"/>
            </View>
          )}
        </View>
        
      </ScrollView>
      {movie.isFetchingNextPage && (
        <View style={styles.loadMoreContainer}>
          <ActivityIndicator color='#FFFFFF' />
          <Text color='#FFFFFF'> Load more movie</Text>
        </View>
      )}
    </View>
  )
}

export default MainScreen

const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'#1d0930',
    paddingHorizontal:20,
  },
  header:{
    paddingTop:20,
    paddingBottom:10,
    flexDirection:'row',
    justifyContent:'space-between',
  },
  profileImage:{
    height:50,
    width:50,
    borderRadius:50,
  },
  topMoviePosterContainer:{
    height:400,
    width:width/1.5,
    marginHorizontal:5,
    borderRadius:20,
    overflow:'hidden',
    padding:20,
    backgroundColor:'#000000'
  },
  movieContainer:{
    marginHorizontal:-20,
    flexDirection:'row',
    flexWrap:'wrap',
    justifyContent:'center'
  },
  topMoviePoster:{
    resizeMode:'contain',
    borderRadius:20,
  },
  moviePoster:{
    resizeMode:'contain',
    borderRadius:20,
    height:270,
    width:width/2-25,
    marginHorizontal:5,
    marginBottom:10,
    overflow:'hidden',
    padding:10,
    justifyContent:'flex-end',
    backgroundColor:'#000000'
  },
  movieTitleContainer:{
    backgroundColor:'#00000090',
    margin:-10,
    padding:10,
  },
  rateContainer:{
    backgroundColor:'#DFA501',
    maxWidth:50,
    height:30,
    justifyContent:'center',
    alignItems:'center',
    borderRadius:10,
  },
  titleContainer:{
    marginVertical:10,
    flexDirection:'row',
    alignItems:'center'
  },
  loadingTopMovie:{
    height:400,
    width:width,
    backgroundColor:'#290c45',
    borderRadius:10,
    justifyContent:'center',
    alignItems:'flex-start',
    paddingLeft:width/2.5        
  },
  loadingMovieList:{
    borderRadius:20,
    height:270,
    width:width,
    backgroundColor:'#290c45',
    marginBottom:20,
    justifyContent:'center',
    alignItems:'flex-start',
    paddingLeft:width/2.5      
  },
  loadMoreContainer:{
    position:'absolute',
    bottom:0,
    left:0,
    right:0,
    height:50,
    backgroundColor:'#00000090',
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center'
  }
})