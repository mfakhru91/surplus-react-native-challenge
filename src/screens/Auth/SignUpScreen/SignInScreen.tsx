import { Image, ScrollView, StyleSheet, TouchableOpacity, View} from 'react-native'
import React, { FC, useState } from 'react'
import { AuthLayout } from '../../../components/organism'
import { Text, TextInput } from '../../../components/atoms'
import {StackScreenProps} from '@react-navigation/stack'
import { RouteParamsList } from '../../../Routes'
import * as yup from 'yup'
import { Formik,ErrorMessage } from 'formik'
import { Button } from 'react-native-paper'
import { Images } from '../../../assets'
import { useAppDispatch } from '../../../redux/hooks'
import { setAuth } from '../../../redux/features/auth/authSlice'
import AsyncStorage from '@react-native-async-storage/async-storage'

type FormValues = {
  username:string
  password:string
  email:string
  confirmPassword:string
}

const SignUpScreen:FC<StackScreenProps<RouteParamsList,"SingUpScreen">> = ({navigation}) => {

  const dispatch = useAppDispatch()

  const [errorMessage, setErrorMessage] = useState("");
  const [initialValues, setInitialValues] = useState<FormValues>({
    "password":"",
    "username":"",
    "email":"",
    "confirmPassword":""
  });

  const validationSchema  = yup.object().shape({
    username:yup.string().required(),
    email:yup.string().email().required(),
    password:yup.string().required(),
    "confirmPassword":yup.string()
      .oneOf([yup.ref("password")])
      .required()
  })

  const rightHeader = () => (
    <TouchableOpacity
      onPress={()=>navigation.navigate("SingInScreen")}>
      <Text color='#FFFFFF'>Sing In</Text>
    </TouchableOpacity>
  )
  
  const onSubmit = (values:FormValues) => {
    try {
      setErrorMessage("")
      const dataAuth = {
        "isLogin":true,
        "username":values.username,
        "email":values.email
      }
      dispatch(setAuth(dataAuth))
      AsyncStorage.setItem("auth",JSON.stringify(dataAuth))
    } catch (error) {
      console.log(error);
    }
  }
  
  
  return (
     <Formik<FormValues>
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}>
          {formikProps => (
            <AuthLayout
              title="Sign Up"
              message="Ready to explore a vast library of films? Sign up and unlock a universe of cinematic wonders on our app!"
              headerRight={rightHeader()}
              goBack={navigation.goBack}>
                <ScrollView showsVerticalScrollIndicator={false}>
                  <Text size={12} color='red'>{errorMessage}</Text>
                  <TextInput
                    style={{marginBottom:10,}}
                    value={formikProps.values.username}
                    onChangeText={formikProps.handleChange("username")}
                    error={!!formikProps.errors.username}
                    errorMessage={formikProps.errors.username}
                    label={"Username"}/>
                  <TextInput
                    style={{marginBottom:10,}}
                    value={formikProps.values.email}
                    onChangeText={formikProps.handleChange("email")}
                    error={!!formikProps.errors.email}
                    keyboardType="email-address"
                    errorMessage={formikProps.errors.email}
                    label={"Email"}/>
                  <TextInput
                    style={{marginBottom:10,}}
                    value={formikProps.values.password}
                    onChangeText={formikProps.handleChange("password")}
                    error={!!formikProps.errors.password}
                    errorMessage={formikProps.errors.password}
                    label={"Password"}
                    password/>
                  <TextInput
                    style={{marginBottom:10,}}
                    value={formikProps.values.confirmPassword}
                    onChangeText={formikProps.handleChange("confirmPassword")}
                    error={!!formikProps.errors.confirmPassword}
                    errorMessage={formikProps.errors.confirmPassword}
                    label={"Confirm Password"}
                    password/>
                  <TouchableOpacity style={{alignSelf:'flex-end'}}>
                    <Text>Reset Password?</Text>
                  </TouchableOpacity>
                  <Button 
                    onPress={formikProps.handleSubmit}
                    style={styles.button}
                    buttonColor='#6A1FB4'
                    textColor='#FFFFFF'>Sign Up</Button>
                  <Button 
                    style={[styles.button,styles.sosmedButton]}
                    mode="elevated">
                      <Image source={Images.googleLogo} style={styles.sosmedLogo}/>
                      <Text> Continue with Google</Text>
                    </Button>
                  <Button 
                    style={[styles.button,styles.sosmedButton]}
                    mode="elevated">
                      <Image source={Images.facebookLogo} style={styles.sosmedLogo}/>
                      <Text> Continue with Google Facebook</Text>
                  </Button>
                </ScrollView>
            </AuthLayout>
          )}
     </Formik>
  )
}

export default SignUpScreen

const styles = StyleSheet.create({
  button:{
    marginVertical:20,
    height:48,
    justifyContent:'center',
  },
  sosmedButton:{
    flexDirection:'row',
    marginVertical:5,
    alignItems:'center'
  },
  sosmedLogo:{
    height:20,
    width:20,    
    resizeMode:"contain"
  }
})