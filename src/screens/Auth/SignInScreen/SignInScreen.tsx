import { Image, StyleSheet, TouchableOpacity, View} from 'react-native'
import React, { FC, useState } from 'react'
import { AuthLayout } from '../../../components/organism'
import { Text, TextInput } from '../../../components/atoms'
import {StackScreenProps} from '@react-navigation/stack'
import { RouteParamsList } from '../../../Routes'
import * as yup from 'yup'
import { Formik,ErrorMessage } from 'formik'
import { Button } from 'react-native-paper'
import { Images } from '../../../assets'
import { useAppDispatch } from '../../../redux/hooks'
import { setAuth } from '../../../redux/features/auth/authSlice'
import AsyncStorage from '@react-native-async-storage/async-storage'

type FormValues = {
  username:string,
  password:string,
  errorMessage?:string
}

const SignInScreen:FC<StackScreenProps<RouteParamsList,"SingInScreen">> = ({navigation}) => {

  const dispatch = useAppDispatch()

  const [errorMessage, setErrorMessage] = useState("");
  const [initialValues, setInitialValues] = useState<FormValues>({
    "password":"",
    "username":"",
  });

  const validationSchema  = yup.object().shape({
    username:yup.string().required(),
    password:yup.string().required()
  })

  const rightHeader = () => (
    <TouchableOpacity
      onPress={()=>navigation.navigate("SingUpScreen")}>
      <Text color='#FFFFFF'>Sing Up</Text>
    </TouchableOpacity>
  )
  
  const onSubmit = (values:FormValues) => {
    try {
      setErrorMessage("")
      if (values.username !== "moviedev" || values.password !== "movie91") {
        setErrorMessage("incorrect username or password")
      }else{
        const dataAuth = {
          "isLogin":true,
          "username":"moviedev",
          "email":"moviedev91@gmail.com"
        }
        AsyncStorage.setItem("auth",JSON.stringify(dataAuth))
        dispatch(setAuth(dataAuth))
      }
    } catch (error) {
      console.log(error);
    }
  }
  
  
  return (
     <Formik<FormValues>
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}>
          {formikProps => (
            <AuthLayout
              title="Sign In"
              message="Ready to dive into the world of movies? Sign in now and embark on an incredible film journey!"
              headerRight={rightHeader()}
              goBack={navigation.goBack}>
                <Text size={12} color='red'>{errorMessage}</Text>
                <TextInput
                  style={{marginBottom:10,}}
                  value={formikProps.values.username}
                  onChangeText={formikProps.handleChange("username")}
                  error={!!formikProps.errors.username}
                  errorMessage={formikProps.errors.username}
                  label={"Username"}/>
                <TextInput
                  style={{marginBottom:10,}}
                  value={formikProps.values.password}
                  onChangeText={formikProps.handleChange("password")}
                  error={!!formikProps.errors.password}
                  errorMessage={formikProps.errors.password}
                  label={"Password"}
                  password/>
                <TouchableOpacity style={{alignSelf:'flex-end'}}>
                  <Text>Reset Password?</Text>
                </TouchableOpacity>
                <Button 
                  onPress={formikProps.handleSubmit}
                  style={styles.button}
                  buttonColor='#6A1FB4'
                  textColor='#FFFFFF'>Sign In</Button>
                <Button 
                  style={[styles.button,styles.sosmedButton]}
                  mode="elevated">
                    <Image source={Images.googleLogo} style={styles.sosmedLogo}/>
                    <Text> Continue with Google</Text>
                  </Button>
                <Button 
                  style={[styles.button,styles.sosmedButton]}
                  mode="elevated">
                    <Image source={Images.facebookLogo} style={styles.sosmedLogo}/>
                    <Text> Continue with Google Facebook</Text>
                  </Button>
            </AuthLayout>
          )}
     </Formik>
  )
}

export default SignInScreen

const styles = StyleSheet.create({
  button:{
    marginVertical:20,
    height:48,
    justifyContent:'center',
  },
  sosmedButton:{
    flexDirection:'row',
    marginVertical:5,
    alignItems:'center'
  },
  sosmedLogo:{
    height:20,
    width:20,    
    resizeMode:"contain"
  }
})