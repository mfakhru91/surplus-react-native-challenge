import { Dimensions, Image, StyleSheet, View } from 'react-native'
import React, { FC } from 'react'
import { Images } from '../../../assets'
import { Text } from '../../../components/atoms'
import { Button } from 'react-native-paper'
import { StackScreenProps } from '@react-navigation/stack'
import { RouteParamsList } from '../../../Routes'

const {width,height} = Dimensions.get("screen")

const AuthScreen:FC<StackScreenProps<RouteParamsList,"AuthScreen">> = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={styles.logoContainer}>
        <Image source={Images.Logo} style={styles.logo}/>
      </View>
      <View style={styles.contentContainer}>
        <Text size={20} font="Poppins-Medium">Welcome</Text>
        <Text style={{textAlign:'justify'}}>Step into a world of cinematic enchantment as we warmly welcome you to our extraordinary film app!</Text>
        <View style={styles.buttonContainer}>
          <Button
            onPress={()=>navigation.navigate('SingInScreen')}
            style={[styles.button,{marginRight:5,}]}
            buttonColor="#000000"            
            mode="contained">Sing In</Button>
          <Button
          onPress={()=>navigation.navigate("SingUpScreen")}
            style={[styles.button,{marginLeft:5,}]}
            buttonColor="#6A1FB4"            
            mode="contained">Sing Up</Button>
        </View>
      </View>
    </View>
  )
}

export default AuthScreen

const styles = StyleSheet.create({
  container:{
    backgroundColor:"#6A1FB4",
    flex:1,
    alignItems:'center'
  },
  logo:{
    height:120,
    width:120,
  },
  logoContainer:{
    justifyContent:'center',
    alignItems:'center',
    flex:1,
  },
  contentContainer:{
    minHeight:height/3,
    width,
    backgroundColor:"#FFFFFF",
    borderTopEndRadius:20,
    borderTopStartRadius:20,
    paddingTop:50,
    padding:20,
    justifyContent:'center',
  },
  buttonContainer:{
    flexDirection:"row",
    justifyContent:'space-between',
    alignItems:'center',
    marginTop:20,
    flex:1,
  },
  button:{
    flex:1,   
    height:48,
    justifyContent:'center', 
  }
})