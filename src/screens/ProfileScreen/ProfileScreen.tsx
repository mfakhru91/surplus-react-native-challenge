import { StyleSheet, View,Image, TouchableOpacity} from 'react-native'
import React, { useState } from 'react'
import {profile_image} from '../../config.json'
import { useAppDispatch, useAppSelector } from '../../redux/hooks'
import { Text } from '../../components/atoms'
import { Button } from 'react-native-paper'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { logout } from '../../redux/features/auth/authSlice'

const ProfileScreen = () => {
  const {auth} = useAppSelector(state=>state)
  const dispatch = useAppDispatch()
  const [isLoading, setIsLoading] = useState(false);

  const onLogout = async () => {
    try {
      setIsLoading(true)
      setTimeout( async() => {
        await AsyncStorage.clear()
        dispatch(logout())
        setIsLoading(false)
      }, 200);
    } catch (error) {
      console.log(error);
    }
  }
  

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Image  source={{uri:profile_image}} style={styles.profileImage}/>
        <Text size={20} font="Poppins-Medium">{auth.username}</Text>
        <Text size={14}>{auth.email}</Text>
        <Button
          buttonColor='#290c45'
          textColor='#FFFFFF'
          loading={isLoading}
          onPress={onLogout}
          style={{marginTop:10,width:"100%"}}
          mode="contained">
          <Text color='#FFFFFF'>Log out</Text>
        </Button>
      </View>
      <View style={styles.menuContainer}>
        <TouchableOpacity style={styles.menuItemContainer}>
          <Icon name='cog' size={20} color={"#290c45"}/>
          <Text style={{marginLeft:20}}>Setting</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.menuItemContainer}>
          <Icon name='credit-card-outline' size={20} color={"#290c45"}/>
          <Text style={{marginLeft:20}}>Payment</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.menuItemContainer}>
          <Icon name='translate' size={20} color={"#290c45"}/>
          <Text style={{marginLeft:20}}>Payment</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default ProfileScreen

const styles = StyleSheet.create({
  container:{
    
  },
  headerContainer:{
    alignItems:'center',
    paddingTop:50,
    paddingHorizontal:20,
  },
  profileImage:{
    height:150,
    width:150,
    borderRadius:150,
    marginBottom:20,
  },
  menuContainer:{
    marginTop:20
  },
  menuItemContainer:{
    flexDirection:'row',
    paddingHorizontal:10,
    paddingVertical:10,
    justifyContent:'space-between', 
    borderBottomWidth:0.5,
    marginHorizontal:20
  }
})