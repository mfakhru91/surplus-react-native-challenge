import { StyleSheet, View, ImageBackground, Dimensions, Image, ScrollView, Linking, TouchableOpacity } from 'react-native'
import React,{FC} from 'react'
import { StackScreenProps } from '@react-navigation/stack'
import { RouteParamsList } from '../../Routes'
import { useQuery } from 'react-query'
import { apiCollection } from '../../services'
import {image_url} from '../../config.json'
import { Text } from '../../components/atoms'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { Button } from 'react-native-paper'
const {width} = Dimensions.get("screen")

const DetailMovieScreen:FC<StackScreenProps<RouteParamsList,"DetailMovieScreen">> = ({
  navigation,
  route
}) => {
  const {id} = route.params
  const detail = useQuery(["detail"],{
    queryFn:()=>apiCollection.movie.getDetailMovie(id)
  })
  
  function formatCompactNumber(number:number) {
    if (number < 1000) {
      return number;
    } else if (number >= 1000 && number < 1_000_000) {
      return (number / 1000).toFixed(1) + "K";
    } else if (number >= 1_000_000 && number < 1_000_000_000) {
      return (number / 1_000_000).toFixed(1) + "M";
    } else if (number >= 1_000_000_000 && number < 1_000_000_000_000) {
      return (number / 1_000_000_000).toFixed(1) + "B";
    } else if (number >= 1_000_000_000_000 && number < 1_000_000_000_000_000) {
      return (number / 1_000_000_000_000).toFixed(1) + "T";
    }
  }

  const rating = () => {
    const vote = Math.ceil(detail.data.vote_average)
    const count:number = detail.data.vote_count||0
    const voteCount = formatCompactNumber(count)
    return(
      <View style={styles.ratingContainer}>        
        <Text color={"#FDC602"}><Icon name='star' color={"#FDC602"} size={14}/> {vote} </Text>
        <Text color='#FFFFFF'>{voteCount}</Text>
      </View>
    )
  }

  const genre = () => {
    const data:any[] = detail.data.genres
    return (
      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
        {data.map((item,index)=>(
          <View 
            style={styles.genreContainer}
            key={index}>
            <Text color='#FFFFFF'>{item.name}</Text>
          </View>
        ))}
      </ScrollView>
    )
  }
  
  const companiesRender = () => {
    const companies:any[] = detail.data.production_companies
    return (
      <View 
      
      style={{marginLeft:10}}>
        {companies.map((item,index)=>(
          <Text key={index} color='#FFFFFF' size={14}><Icon name='circle'/> {item.name}</Text>
        ))}
      </View>
    )
  }
  
  

  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={navigation.goBack}
        style={styles.goBackButton}>
        <Icon color={"#FFFFFF"} size={20} name='chevron-left' />
      </TouchableOpacity>
      {!detail.isLoading && !detail.isRefetching ? (
        <ScrollView showsVerticalScrollIndicator={false}>
          <ImageBackground
            style={styles.posterContainer}
            imageStyle={styles.posterImageContainer}
            blurRadius={10}
            source={{uri:`${image_url}${detail.data.poster_path}`}}>
              <View style={{flexDirection:'row', alignItems:'flex-end', marginBottom:10}}>
                <Image 
                  style={styles.posterImage}
                  source={{uri:`${image_url}${detail.data.poster_path}`}}/>
                <View>
                  {rating()}
                </View>
              </View>
              <Text color="#FFFFFF" size={25} font="Poppins-Medium">{detail.data.original_title}</Text>
            {genre()}
          </ImageBackground>
          <View style={styles.body}>
            <Text size={18} color='#FFFFFF' font="Poppins-Bold">Overview:</Text>
            <Text size={15} color='#FFFFFF' style={{textAlign:'justify',marginLeft:10,}}>{detail.data.overview}</Text>

            <Text size={18} color='#FFFFFF' font="Poppins-Bold">Production Companies:</Text>
            {companiesRender()}

            <Button 
              onPress={()=>Linking.openURL(detail.data.homepage)}
              mode="outlined"
              icon={"movie"}
              textColor='#FFFFFF'
              style={{marginTop:50,borderColor:"#FFFFFF"}}>
              <Text color='#FFFFFF'>Watch now</Text>
            </Button>
          </View>
        </ScrollView>
      ):(
        <View style={[styles.posterContainer,{backgroundColor:"#290c45"}]}>

        </View>
      )}
    </View>
  )
}

export default DetailMovieScreen

const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'#1d0930',
  },
  posterContainer:{
    minHeight:width,    
    paddingTop:70,
    paddingBottom:20,
    paddingHorizontal:20
  },
  posterImageContainer:{
        
  },
  posterImage:{
    height:200,
    width:140,
    resizeMode:'contain',
    borderRadius:10,    
  },
  ratingContainer:{
    flexDirection:'row',
    alignItems:'center',
    marginLeft:20
  },
  genreContainer:{
    padding:10,
    backgroundColor:'#00000090',
    height:40,
    marginRight:10,
    borderRadius:10,
  },
  body:{
    padding:20
  },
  goBackButton:{
    backgroundColor:"#00000090",
    height:50,
    width:50,
    borderRadius:50,
    position:'absolute',
    top:10,
    left:20,
    zIndex:10,
    marginBottom:20,
    justifyContent:'center',
    alignItems:'center'
  }
})