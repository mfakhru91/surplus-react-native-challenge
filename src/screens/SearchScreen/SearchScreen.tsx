import { Dimensions, ScrollView, StyleSheet, View } from 'react-native'
import React,{FC, useState} from 'react'
import { useInfiniteQuery } from 'react-query'
import { apiCollection } from '../../services'
import { TouchableOpacity } from 'react-native'
import { Image } from 'react-native'
import {image_url, profile_image} from '../../config.json'
import { ImageBackground } from 'react-native'
import { Text, TextInput } from '../../components/atoms'
import { useAppSelector } from '../../redux/hooks'
import {ActivityIndicator, TextInput as Input} from 'react-native-paper'
import { StackScreenProps } from '@react-navigation/stack'
import { RouteParamsList } from '../../Routes'

const {width, height} = Dimensions.get("screen")


const SearchScreen:FC<StackScreenProps<RouteParamsList,"SearchScreen">> = ({
  navigation,
  route
}) => {
  const {key} = route.params
  const [search, setSearch] = useState(key);
  const {auth} = useAppSelector(state=>state)

  const movie = useInfiniteQuery(["movieSearch"],{
    queryFn:({pageParam = 1})=>apiCollection.movie.searchMovieList({
      page:pageParam,
      search}),
    getNextPageParam:(lastPage)=> {
      if (!lastPage.hasNextPage) return null
      return lastPage.nextPage
    }
  })

  const movieList = movie.data?.pages.flatMap((page) => page.data.results);

  const hendleScroll = (event:any) => {
    const offsetY = event.nativeEvent.contentOffset.y;
    const contentHeight = event.nativeEvent.contentSize.height;
    const scrollHeight = event.nativeEvent.layoutMeasurement.height;
    if (offsetY + scrollHeight >= contentHeight) {
      if (!movie.isLoading && !movie.isRefetching && !movie.isFetchingNextPage) {
        movie.fetchNextPage()
      }
    }
  }

  const movieRender = ({item,index}:{item:any,index:any}) => {    
    return (
      <TouchableOpacity 
        onPress={()=>navigation.navigate("DetailMovieScreen",{id:item.id})}
        key={index}>
        <ImageBackground
          style={styles.moviePoster}
          source={{uri:`${image_url}${item.poster_path}`}}>
          <View style={styles.movieTitleContainer}>
            <Text color='#FFFFFF' >{item.original_title}</Text>
            <View style={styles.rateContainer}>
                <Text>{item.vote_average}</Text>
            </View>
          </View>
        </ImageBackground>
      </TouchableOpacity>
    )
  }

  return (
    <View style={styles.container}>
    <View style={styles.header}>
      <View>
        <Text color='#FFFFFF'><Text font="Poppins-Bold" color='#FFFFFF'>Hallo</Text> {auth.username}</Text>
        <Text color='#FFFFFF' font="Poppins-Light" size={12}>look for your favorite movie</Text>
      </View>
      <Image style={styles.profileImage} source={{uri:profile_image}} />
    </View>
    
    <ScrollView
      onScroll={hendleScroll}
      showsVerticalScrollIndicator={false}>

      <TextInput
        mode="outlined"
        placeholder='Search Movies'
        value={search}
        onChangeText={text=>setSearch(text)}
        onEndEditing={()=>search !== "" && movie.refetch()}
        right={<Input.Icon  
          onPress={()=>search !== "" && movie.refetch()}
          icon={"magnify"} color={"#000000"}/>}/>  
      
      <View style={styles.movieContainer}>
        {!movie.isLoading && !movie.isRefetching ? movieList?.map((item,index)=>(
          movieRender({index,item})
        )):(
          <View style={styles.loadingMovieList}>
            <ActivityIndicator size="large"/>
          </View>
        )}
      </View>
      
    </ScrollView>
    {movie.isFetchingNextPage && (
      <View style={styles.loadMoreContainer}>
        <ActivityIndicator color='#FFFFFF' />
        <Text color='#FFFFFF'> Load more movie</Text>
      </View>
    )}
  </View>
  )
}

export default SearchScreen

const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'#1d0930',
    paddingHorizontal:20,
  },
  header:{
    paddingTop:20,
    paddingBottom:10,
    flexDirection:'row',
    justifyContent:'space-between',
  },
  profileImage:{
    height:50,
    width:50,
    borderRadius:50,
  },
  topMoviePosterContainer:{
    height:400,
    width:width/1.5,
    marginHorizontal:5,
    borderRadius:20,
    overflow:'hidden',
    padding:20,
    backgroundColor:'#000000'
  },
  movieContainer:{
    marginHorizontal:-20,
    flexDirection:'row',
    flexWrap:'wrap',
    justifyContent:'center'
  },
  topMoviePoster:{
    resizeMode:'contain',
    borderRadius:20,
  },
  moviePoster:{
    resizeMode:'contain',
    borderRadius:20,
    height:270,
    width:width/2-25,
    marginHorizontal:5,
    marginBottom:10,
    overflow:'hidden',
    padding:10,
    justifyContent:'flex-end',
    backgroundColor:'#000000'
  },
  movieTitleContainer:{
    backgroundColor:'#00000090',
    margin:-10,
    padding:10,
  },
  rateContainer:{
    backgroundColor:'#DFA501',
    maxWidth:50,
    height:30,
    justifyContent:'center',
    alignItems:'center',
    borderRadius:10,
  },
  titleContainer:{
    marginVertical:10,
    flexDirection:'row',
    alignItems:'center'
  },
  loadingMovieList:{
    borderRadius:20,
    height:height-230,
    width:width,
    marginBottom:20,
    justifyContent:'center',
    alignItems:'center'    
  },
  loadMoreContainer:{
    position:'absolute',
    bottom:0,
    left:0,
    right:0,
    height:50,
    backgroundColor:'#00000090',
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center'
  }
})