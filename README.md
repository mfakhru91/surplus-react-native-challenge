## Developing
**Clone this repository**
```bash
$ git clone https://gitlab.com/mfakhru91/surplus-react-native-challenge
```

**Install**
```bash
$ cd surplus-react-native-challenge
$ npm install or yarn install
```


### Running on Android simulator
* Install depedency `yarn install`
* Run `yarn start` and `yarn android`


### Account Login
* Email : `moviedev`
* Password: `movie91`
